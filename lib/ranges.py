#!/usr/bin/python

# gets range buckets

### a note about buckets..  now that we have them coded out, I dont know why we split them into the ranges 

import sys
import os

def build_range(input_file):
	ranges_raw = open(os.environ['SMALLDATA_HOME'] + '/config/' + input_file,'r')
	range_s = []
	for i in ranges_raw:
		r = i.split(",")
		range_s.append(range(int(r[0]),int(r[1])))

	return range_s

def get_lead_buckets():
	lead_ranges_raw = 'lead_ranges.ref'
	return build_range(lead_ranges_raw)

def get_ldp_buckets():
	ldp_ranges_raw = 'ldp_ranges.ref'
	return build_range(ldp_ranges_raw)


