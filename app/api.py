#!/usr/bin/python
import sys
import os
sys.path.append(os.environ['SMALLDATA_HOME'] + '/app')
sys.path.append(os.environ['SMALLDATA_HOME'] + '/lib')
import app as Bitcalc
import ranges as Ranges

from flask import Flask
from flask import jsonify
app = Flask(__name__)


@app.route("/test_json")
def test():
		testval = {}
		testval['test'] = "This is the test route"
		return jsonify(testval)

@app.route("/get_ranges/<string:source>",methods =['GET'])
def get_range(source):
	if source == 'LDP':
		range_bucket = Ranges.get_ldp_buckets()
	elif source == 'LEADS':
		range_bucket = Ranges.get_lead_buckets()
	else:
		raise

	range_fix = [ str(i[0]) + "-" + str(i[-1]) for i in range_bucket ]
	range_out = {"range" : range_fix}
	return jsonify(range_out)
	
#@app.route("/calc_complex", methods=['POST'])
@app.route("/calc_complex")
def calc():
	#data = request.form['data']
	data = [[[[[1,2,4],'51-60','IN'],[[7,10,16],'51-60','IN']], 'OR'],[[[[1,2,4],'1-3','GT'],[[7,10,16],'1-3','IN']], 'OR'], 'AND']
	#data = eval(data_in)
	result = Bitcalc.calc_complex(*data)
	return jsonify(result)

if __name__ == "__main__":
    app.run(debug=True)

