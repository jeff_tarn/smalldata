#!/usr/bin/python

import sys
import os
sys.path.append(os.environ['SMALLDATA_HOME'] + '/lib')

import ranges as Ranges
import connect_redis as Redis
import uuid

# bitOP => AND, OR
# bucketOP => IN, GT, LT 
# [[[[1,2,4],bucket,bucketop],[[7,10,16],bucket,bucketop]], BITOP]

# [[[1],'1-10','GT'],'OR']    ldp_list = [[[[1],'1-10','GT']],'OR']

# def calc_simple(ldp_list,merge_bitop):
# 	redis_conn = Redis.connect()
# 	ldp_op = ldp_list[1]
def calc_simple(source, days, merge_bitop):

	# source to be LDP/LEADS
	# no buckets
	new_bit = str(uuid.uuid4())
	redis_conn = Redis.connect()
	day_list = [ source + ':201404' + str(i).zfill(2) for i in days ]
	redis_conn.bitop(merge_bitop, new_bit, *day_list)

	redis_conn.expire(new_bit,30)
	return redis_conn.bitcount(new_bit)

def calc_complex(ldp_list, lead_list, merge_bitop):
	
	redis_conn = Redis.connect()
	
	# ldp bitop
	ldp_bit_id = 'LDP' + str(uuid.uuid4())
	ldp_op = ldp_list[1]
	ldp_list.pop()
	ldp_bit_list = [ find_bits( 'LDP',y[1],y[2],y[0]) for x in ldp_list for y in x  ]
	redis_conn.bitop(ldp_op,ldp_bit_id,*[ y for x in ldp_bit_list for y in x])

	lead_bit_id = 'LEAD' + str(uuid.uuid4())
	lead_op = lead_list[1]
	lead_list.pop()
	lead_bit_list = [ find_bits('LEAD',y[1],y[2],y[0]) for x in lead_list for y in x ]
	redis_conn.bitop(lead_op,lead_bit_id,*[ y for x in lead_bit_list for y in x])

	merged_bit_id = 'MER' + str(uuid.uuid4())
	redis_conn.bitop(merge_bitop,merged_bit_id,ldp_bit_id,lead_bit_id)

	count = {}
	count['ldp'] = redis_conn.bitcount(ldp_bit_id)
	count['leads'] = redis_conn.bitcount(lead_bit_id)
	count['merged'] = redis_conn.bitcount(merged_bit_id)

	# expire these so repeated jobs dont take space
	redis_conn.expire(ldp_bit_id,30)
	redis_conn.expire(lead_bit_id,30)
	redis_conn.expire(merged_bit_id,30)

	return count

def find_bits(source, bucket, bucketOP, qlist):
	# calc ldp first
	# source should be "LDP" or "LEAD"

	rel_dates = [ '201404' + str(j).zfill(2) for j in qlist ]
	rel_buckets = get_calc_buckets(source, bucket, bucketOP) 
	bitarrays = [ source + ":" + x + ":" + y for x in rel_dates for y in rel_buckets ]
	return bitarrays


def get_calc_buckets(source, bucket, bucketOP):  ### IN (in), GT (greater_than), LT (less_than)
	## the bucketOP will be inclusive
	if bucketOP == 'IN':
		return [bucket] # no need to calc anything.. just kick it back
	else:
		if source == 'LDP':
			buckets_raw =  Ranges.get_ldp_buckets()  
		else:
			buckets_raw = Ranges.get_lead_buckets()

		bucket_ref = [ str(i[0]) + "-" + str(i[-1]) for i in buckets_raw ]
		
		try:
			bucket_index = bucket_ref.index(bucket)
		except:
			raise

		if bucketOP == 'GT':
			btm_range = bucket_index
			top_range = len(bucket_ref)
		else:
			btm_range = 0
			top_range = bucket_index + 1

		required_buckets = [ bucket_ref[i] for i in range( btm_range, top_range ) ]

		return required_buckets




