#!/usr/bin/python

import sys
import os
sys.path.append(os.environ['SMALLDATA_HOME'] + '/app')
sys.path.append(os.environ['SMALLDATA_HOME'] + '/lib')
import app as App
import named_date as ND

def test_buckets_in():
	bucket_in = App.get_calc_buckets('LDP','51-60','IN')

	assert len(bucket_in) == 1

def test_buckets_gt_i():
	#  test inclusion
	bucket_in = App.get_calc_buckets('LDP','51-60','GT')

	assert bucket_in.index('51-60') >= 0

def test_buckets_lt_i():
	#  test inclusion
	bucket_in = App.get_calc_buckets('LDP','51-60','LT')

	assert bucket_in.index('51-60') >= 0

def test_make_keys():
	# test args in. confirm that this returns bit keys
	bits = App.find_bits('LDP','51-60','GT',[1,2,3])

	assert len(bits) >= 0

def test_calc_complex():
	ldp_in = [[[[1,2,4],'51-60','IN'],[[7,10,16],'51-60','IN']], 'OR']
	lead_in = [[[[1,2,4],'1-3','GT'],[[7,10,16],'1-3','IN']], 'OR']
	mer_bo = 'AND'
	calc = App.calc_complex(ldp_in,lead_in,mer_bo)

	assert type(calc) == dict

