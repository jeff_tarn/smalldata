#!/usr/bin/python

import sys
import os
sys.path.append(os.environ['SMALLDATA_HOME'] + '/lib')


## add dependent modules
import named_date as ND
import ranges as Ranges
import connect_redis as Redis


def test_ldp_range():
	ldp_range = Ranges.get_ldp_buckets()

	assert ldp_range >= 0

def test_lead_range():
	lead_range = Ranges.get_lead_buckets()

	assert lead_range >= 0

def test_redis_connect():
	## just confirms some connection
	r = Redis.connect()
	time = r.time()

	assert int(time[0]) >= 0

def test_weekend():
	wnd = ND.nd('weekdend')
	assert len(wnd) > 0

def test_weekday():
	wkd = ND.nd('weekday')
	assert len(wkd) > 0