# we gonna win...

## what we are doing
###stealing a good idea...
1. http://blog.getspool.com/2011/11/29/fast-easy-realtime-metrics-using-redis-bitmaps/

## current dependencies
1. redis (http://redis.io/download)
1. python2.7
1. pip
1. redis-py (sudo pip install redis-py)
1. flask (sudo pip install flask)
1. pytest (sudo pip install pytest)

## use it yourself
1. install dependencies
1. clone project and navigate to project dir
1. navigate to ./data/ 
1. start redis server (redis-server &)
1. start flask (source ./lib/init; ./app/api.py)
1. run tests (source ./lib/init; cd ./test; py.test)
1. open ./site/index.html
1. derive insight through smallData 

## ipython
1. there is a demo ipython notebook thats pretty handy - http://ipython.org/install.html

# you dont need this to play with the hack..  but if you wanted to seed some other data, continue reading

## real seeding  -  some of this could be sped up with hadoop streaming, but thats defeating our point
1. run hive extracts
1. start redis
1. key the users (source ./lib/init; cat $USERS |./setup/key_init.py 0 > ./tmp/users.txt)
1. seed the user lookup hash (source ./lib/init; cat ./tmp/users.txt |./setup/load_users.py)
1. resolve users on ldp xacts (source ./lib/init; cat $LDPS |./setup/resolve_user_key.py |grep -v '^$' > ./tmp/ldps.txt)
1. resolve users on lead xacts (source ./lib/init; cat $LEADS |./setup/resolve_user_key.py |grep -v '^$' > ./tmp/leads.txt)
1. seed LDP events (source ./lib/init; cat ./tmp/ldps.txt | ./setup/seed_events.py LDP)
1. seed lead events (source ./lib/init; cat ./tmp/leads.txt|grep -v None | ./setup/seed_events.py LEAD)
1. clean out tmp


## testing logic
1. seed known vals for logic testing (source ./lib/init; ./test/seed.py)

## aggregation (has not been flushed out really)
1. basic aggregation (source ./lib/init; ./setup/agg/agg_bits.py)