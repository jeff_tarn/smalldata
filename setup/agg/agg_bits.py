#!/usr/bin/python

import sys
import os
sys.path.append(os.environ['SMALLDATA_HOME'] + '/app')
sys.path.append(os.environ['SMALLDATA_HOME'] + '/lib')
import ranges as Ranges
import connect_redis as Redis
import app as App

redis_conn = Redis.connect()

for i in range(1,32):
	ldp_list = [[[[i],'1-10','GT']],'OR']
	ldp_op = ldp_list[1]
	ldp_list.pop()
	ldp_bit_list = [ App.find_bits( 'LDP',y[1],y[2],y[0]) for x in ldp_list for y in x  ]
	new_ldp_bit ='LDP' + ':201404' +str(i).zfill(2)
	print new_ldp_bit
	redis_conn.bitop(ldp_op,new_ldp_bit,*[ y for x in ldp_bit_list for y in x])

for i in range(1,32):
	lead_list = [[[[i],'1-3','GT']],'OR']
	lead_op = lead_list[1]
	lead_list.pop()
	lead_bit_list = [ App.find_bits( 'LEAD',y[1],y[2],y[0]) for x in lead_list for y in x  ]
	new_lead_bit ='LEAD' + ':201404' +str(i).zfill(2)
	print new_lead_bit
	redis_conn.bitop(lead_op,new_lead_bit,*[ y for x in lead_bit_list for y in x])