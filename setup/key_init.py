#!/usr/bin/python

# pipe in distinct users to gen space delim output. 

# 1st arg is number to start with.

# like `cat users|/\./key_init.py 0`

import sys

def main():
	incr = int(sys.argv[1]) + 1
	delim = " "
	for line in sys.stdin:
		val = str(incr) + delim + line
		sys.stdout.write(val)
		incr += 1



if __name__ == "__main__":
    main()