#!/usr/bin/python

# load redis hash for user.
# expects format of userINT, userDICT

import sys
import os
sys.path.append(os.environ['SMALLDATA_HOME'] + '/lib')

import connect_redis

def main():
	
	delim = " "
	redis_conn = connect_redis.connect()

	for line in sys.stdin:
		data = line.split(" ")
		redis_conn.hset('user', str(data[1]).strip() , int(data[0].strip()))



if __name__ == "__main__":
    main()