#!/usr/bin/python

# bitkey is LDP:DATE:RANGE
# LDP:20140428:11-20

import sys
import os
sys.path.append(os.environ['SMALLDATA_HOME'] + '/lib')

import ranges
import connect_redis
redis_conn = connect_redis.connect()

source = sys.argv[1]

## I dont know python that well, but this is certainly annoying.
def check_range(range_bucket,count):
	try:
		rb =range_bucket.index(count)
		return rb
	except:
		return -1

for line in sys.stdin:
	data = line.split(" ")
	logdate = data[0]
	user = int(data[1])
	count = int(data[2])
	if source == 'LDP':
		range_bucket = ranges.get_ldp_buckets()
	else:
		range_bucket = ranges.get_lead_buckets()

	key_prefix = source + ":" + logdate + ":" 

	# just update 1 bucket, rather than cascade down. this will take forever.  better to take the hit on the query
	# for i in range_bucket:
	# 	if count >= int(i[0]):
	# 		 bitkey = key_prefix + str(i[0]) + "-" + str(i[-1])
	# 		 redis_conn.setbit(bitkey, user, 1)
	# 	else:
	# 		pass

	bitkey = [ key_prefix + str(i[0]) + '-' + str(i[-1]) for i in range_bucket if check_range(i,count) > -1 ]
	if len(bitkey)>0:
		redis_conn.setbit(bitkey[0], user, 1)


