#!/usr/bin/python

# creates output from pipe where userHASH is replaced with the userINT.

import sys
import os
sys.path.append(os.environ['SMALLDATA_HOME'] + '/lib')

import connect_redis

def main():
	
	delim = "\t"
	redis_conn = connect_redis.connect()

	for line in sys.stdin:
		try:
			data = line.split(delim)
			res = redis_conn.hget('user', str(data[1].strip()))
			data[1] = str(res)
			print " ".join(data)
		except:
			pass

if __name__ == "__main__":
    main()